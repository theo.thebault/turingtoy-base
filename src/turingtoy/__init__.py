from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)

def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    blank_symbol = machine["blank"]
    initial = machine["start state"]
    final = machine["final states"]
    states = machine["table"].keys()
    transition_table = machine["table"]
    step = 0
    tape: list[str] = list(input_)
    head_position = 0
    execution_history = []
    current = initial

    while current not in final:
        if steps is not None and step >= steps:
            break

        if head_position < 0:
            tape.insert(0, blank_symbol)
            head_position = 0
            
        elif head_position >= len(tape):
            tape.append(blank_symbol)

        symbol = tape[head_position]

        if symbol not in transition_table[current]:
            break

        transition = transition_table[current][symbol]
        history_dict = { 'state': current,'reading': symbol,'position': head_position,'memory': ''.join(tape),'transition': transition}
        execution_history.append(history_dict)

        if transition == "L":
            head_position -= 1
        elif transition == "R":
            head_position += 1

        else:
            if "write" in transition:
                tape[head_position] = transition['write']
                
            if "L" in transition:
                head_position -= 1
                current = transition['L']
                
            elif "R" in transition:
                head_position += 1
                current = transition['R']
                
            else:
                head_position += 0
                break
        step += 1

    result = "".join(tape).strip(blank_symbol)
    return (result, execution_history, current in final)
